const form = document.querySelector("form");
const inputs = document.querySelectorAll(".form-input");
const errorMessage = document.querySelectorAll(".invalid-feedback");

const validationRules = {
  name: /[A-Za-z]+/g,
  email: /^[^ ]+@[^ ]+\.[a-z]{2,3}$/g,
  phone: /\+[0-9]{12}$/g,
  message: /^.{10,}$/g,
};

function formValidation() {
  inputs.forEach((input, i) => {
    const validationKey = input.getAttribute("validation");
    const validationRule = validationRules[validationKey];
    const isValid = input.value.match(validationRule);

    if (isValid) {
      input.classList.add("valid-form");
      input.classList.remove("invalid-form");
      errorMessage[i].style.display = "none";
    } else {
      input.classList.remove("valid-form");
      input.classList.add("invalid-form");
      errorMessage[i].style.display = "block";
    }
  });
}

function showInputsData() {
  const formData = new FormData(form);
  const inputsData = `first name: ${formData.get("firstname")}
last name: ${formData.get("lastname")}
email: ${formData.get("email")}
phone number: ${formData.get("phonenumber")}
message: ${formData.get("message")}`;

  console.log(inputsData);
}

form.addEventListener("submit", (e) => {
  e.preventDefault();
  formValidation();
  showInputsData();
});
